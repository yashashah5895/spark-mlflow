from pyspark.ml.classification import RandomForestClassifier
from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.sql.types import *
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.sql.functions import *
from pyspark.ml.feature import VectorAssembler
import mlflow
import mlflow.spark

spark = SparkSession.builder.appName("random_for").getOrCreate()

print("MLflow Version:", mlflow.version.VERSION)
print("Tracking URI:", mlflow.tracking.get_tracking_uri())


from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("--experiment_name", dest="experiment_name", help="experiment_name", default="RFpyspark", required=False)
parser.add_argument("--data_path", dest="data_path", help="data_path", required=True)
parser.add_argument("--max_depth", dest="max_depth", help="max_depth", default=5, type=int)
parser.add_argument("--numTrees", dest="numTrees", help="numTrees", default=3, type=int)
parser.add_argument("--describe", dest="describe", help="Describe data", default=False, action='store_true')
args = parser.parse_args()



client = mlflow.tracking.MlflowClient()
print("experiment_name:",args.experiment_name)
mlflow.set_experiment(args.experiment_name)
print("experiment_id:",client.get_experiment_by_name(args.experiment_name).experiment_id)

columns = ['Elevation', 'Aspect', 'Slope', 'Horizontal_Distance_To_Hydrology',
       'Vertical_Distance_To_Hydrology', 'Horizontal_Distance_To_Roadways',
       'Hillshade_9am', 'Hillshade_Noon', 'Hillshade_3pm',
       'Horizontal_Distance_To_Fire_Points', 'Wilderness_Area1',
       'Wilderness_Area2', 'Wilderness_Area3', 'Wilderness_Area4',
       'Soil_Type1', 'Soil_Type2', 'Soil_Type3', 'Soil_Type4', 'Soil_Type5',
       'Soil_Type6', 'Soil_Type7', 'Soil_Type8', 'Soil_Type9', 'Soil_Type10',
       'Soil_Type11', 'Soil_Type12', 'Soil_Type13', 'Soil_Type14',
       'Soil_Type15', 'Soil_Type16', 'Soil_Type17', 'Soil_Type18',
       'Soil_Type19', 'Soil_Type20', 'Soil_Type21', 'Soil_Type22',
       'Soil_Type23', 'Soil_Type24', 'Soil_Type25', 'Soil_Type26',
       'Soil_Type27', 'Soil_Type28', 'Soil_Type29', 'Soil_Type30',
       'Soil_Type31', 'Soil_Type32', 'Soil_Type33', 'Soil_Type34',
       'Soil_Type35', 'Soil_Type36', 'Soil_Type37', 'Soil_Type38',
       'Soil_Type39', 'Soil_Type40', 'Cover_Type']

columns_struct_fields = [StructField(field_name, IntegerType(), True)
                                 for field_name in columns]

schema = StructType(columns_struct_fields)
data=spark.read.csv(args.data_path,header=True,schema=schema)
if (args.describe):
        print("==== Data")
        data.show(10)

def train(data, maxDepth, numTrees, run_id):
    dividedData = data.randomSplit([0.7, 0.3], seed=12345) 
    trainingData = dividedData[0] #index 0 = data training
    testingData = dividedData[1] #index 1 = data testing
    
    # MLflow - log parameters
    print("Parameters:")
    print("  maxDepth:",maxDepth)
    print("  numTrees:",numTrees)
    mlflow.log_param("maxDepth",maxDepth)
    mlflow.log_param("numTrees",numTrees)

    assembler = VectorAssembler(inputCols = columns[:-1], outputCol="features")
    trainingDataFinal = assembler.transform(trainingData).select(col("features"), col("Cover_Type").alias("label"))
    trainingDataFinal.show(truncate=False, n=7)
    
    testingDataFinal = assembler.transform(testingData).select(col("features"), col("Cover_Type").alias("trueLabel"))
    
    random_f = RandomForestClassifier(numTrees=numTrees, maxDepth=maxDepth, seed=42, labelCol="label",featuresCol="features")
    random_f = random_f.fit(trainingDataFinal)
    predictions = random_f.transform(testingDataFinal)
    
	#predictionFinal = prediction.select("features","prediction","probability","trueLabel")
    
	#correctPrediction = predictionFinal.filter(predictionFinal['prediction'] == predictionFinal['trueLabel']).count()
    #totalData = predictionFinal.count()
    #print("correct_prediction:", correctPrediction,"total data:",totalData)
    #mlflow.log_metric('correct_prediction',correctPrediction)
    #mlflow.log_metric('total_data',totalData)
          
    
    metrics = ["accuracy","weightedRecall","weightedPrecision","f1"]
    for metric in metrics:
        evaluator = MulticlassClassificationEvaluator(labelCol="trueLabel", predictionCol="prediction", metricName=metric)
        v = evaluator.evaluate(predictions)
        print("  {}: {}".format(metric,v))
        mlflow.log_metric(metric,v)
    
    # MLflow - log model
    mlflow.spark.log_model(random_f, "spark-RFmodel")


    
with mlflow.start_run() as run:
        print("MLflow:")
        print("run_id:",run.info.run_id)
        print("experiment_id:",run.info.experiment_id)
        train(data, args.max_depth, args.numTrees, run.info.run_id)   