import mlflow
import mlflow.spark
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

print("MLflow Version:", mlflow.version.VERSION)
print("Tracking URI:", mlflow.tracking.get_tracking_uri())

from argparse import ArgumentParser
parser = ArgumentParser()
#parser.add_argument("--run_id", dest="run_id", help="run_id", required=True)
parser.add_argument("--data_path", dest="data_path", help="data_path", required=True)
parser.add_argument("--model", dest="model", help="pass model",required = True)
args = parser.parse_args()

spark = SparkSession.builder.appName("Predict").getOrCreate()

columns = ['Elevation', 'Aspect', 'Slope', 'Horizontal_Distance_To_Hydrology',
           'Vertical_Distance_To_Hydrology', 'Horizontal_Distance_To_Roadways',
           'Hillshade_9am', 'Hillshade_Noon', 'Hillshade_3pm',
           'Horizontal_Distance_To_Fire_Points', 'Wilderness_Area1',
           'Wilderness_Area2', 'Wilderness_Area3', 'Wilderness_Area4',
           'Soil_Type1', 'Soil_Type2', 'Soil_Type3', 'Soil_Type4', 'Soil_Type5',
           'Soil_Type6', 'Soil_Type7', 'Soil_Type8', 'Soil_Type9', 'Soil_Type10',
           'Soil_Type11', 'Soil_Type12', 'Soil_Type13', 'Soil_Type14',
           'Soil_Type15', 'Soil_Type16', 'Soil_Type17', 'Soil_Type18',
           'Soil_Type19', 'Soil_Type20', 'Soil_Type21', 'Soil_Type22',
           'Soil_Type23', 'Soil_Type24', 'Soil_Type25', 'Soil_Type26',
           'Soil_Type27', 'Soil_Type28', 'Soil_Type29', 'Soil_Type30',
           'Soil_Type31', 'Soil_Type32', 'Soil_Type33', 'Soil_Type34',
           'Soil_Type35', 'Soil_Type36', 'Soil_Type37', 'Soil_Type38',
           'Soil_Type39', 'Soil_Type40', 'Cover_Type']

columns_struct_fields = [StructField(field_name, IntegerType(), True) for field_name in columns]

schema = StructType(columns_struct_fields)

#run_id = 'df27b901124c4c059f88e1cd05347b7c'
data=spark.read.csv(args.data_path,header=True,schema=schema)

#model_uri = r"{}/{}/spark-RFmodel".format(mlflow.tracking.get_tracking_uri(),run_id)
#model_uri = f"runs:/{args.run_id}/spark-model"
#print("model_uri:",model_uri)
#model = mlflow.spark.load_model(model_uri)
#model_uri = "{}/{}/spark-RFmodel".format(mlflow.tracking.get_tracking_uri(),run_id)
#print("model_uri:",model_uri)

model = args.model
model = mlflow.spark.load_model(model)

assembler = VectorAssembler(inputCols = columns[:-1], outputCol="features")
trainingDataFinal = assembler.transform(data).select(col("features"), col("Cover_Type").alias("label"))
    
predictions = model.transform(trainingDataFinal)
metrics = ["accuracy","weightedRecall","weightedPrecision","f1"]

for metric in metrics:
    evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction", metricName=metric)
    v = evaluator.evaluate(predictions)
    print("{}: {}".format(metric,v))
    mlflow.log_metric(metric,v)
    

