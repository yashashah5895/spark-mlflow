from pyspark.ml.classification import LogisticRegression
from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.sql.types import *
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.sql.functions import *
from pyspark.ml.feature import VectorAssembler
import mlflow
import mlflow.spark
import numpy as np

spark = SparkSession.builder.appName("logistic_reg").getOrCreate()

print("MLflow Version:", mlflow.version.VERSION)
print("Tracking URI:", mlflow.tracking.get_tracking_uri())



from argparse import ArgumentParser



parser = ArgumentParser()
parser.add_argument("--experiment_name", dest="experiment_name", help="experiment_name", default="log_reg_pyspark", required=False)
parser.add_argument("--data_path", dest="data_path", help="data_path", required=True)
parser.add_argument("--parameter", dest="parameter", help="parameter", required=True)
parser.add_argument("--describe", dest="describe", help="Describe data", default=False, action='store_true')
args = parser.parse_args()



client = mlflow.tracking.MlflowClient()
print("experiment_name:",args.experiment_name)
mlflow.set_experiment(args.experiment_name)
print("experiment_id:",client.get_experiment_by_name(args.experiment_name).experiment_id)


columns = ['Elevation', 'Aspect', 'Slope', 'Horizontal_Distance_To_Hydrology',
       'Vertical_Distance_To_Hydrology', 'Horizontal_Distance_To_Roadways',
       'Hillshade_9am', 'Hillshade_Noon', 'Hillshade_3pm',
       'Horizontal_Distance_To_Fire_Points', 'Wilderness_Area1',
       'Wilderness_Area2', 'Wilderness_Area3', 'Wilderness_Area4',
       'Soil_Type1', 'Soil_Type2', 'Soil_Type3', 'Soil_Type4', 'Soil_Type5',
       'Soil_Type6', 'Soil_Type7', 'Soil_Type8', 'Soil_Type9', 'Soil_Type10',
       'Soil_Type11', 'Soil_Type12', 'Soil_Type13', 'Soil_Type14',
       'Soil_Type15', 'Soil_Type16', 'Soil_Type17', 'Soil_Type18',
       'Soil_Type19', 'Soil_Type20', 'Soil_Type21', 'Soil_Type22',
       'Soil_Type23', 'Soil_Type24', 'Soil_Type25', 'Soil_Type26',
       'Soil_Type27', 'Soil_Type28', 'Soil_Type29', 'Soil_Type30',
       'Soil_Type31', 'Soil_Type32', 'Soil_Type33', 'Soil_Type34',
       'Soil_Type35', 'Soil_Type36', 'Soil_Type37', 'Soil_Type38',
       'Soil_Type39', 'Soil_Type40', 'Cover_Type']
columns_struct_fields = [StructField(field_name, IntegerType(), True)
                                 for field_name in columns]

schema = StructType(columns_struct_fields)
data=spark.read.csv(args.data_path,header=True,schema=schema)
if (args.describe):
        print("==== Data")
        data.show(10)
		
		
def train(data, maxIter,aggregationDepth,regParam,run_id):
    dividedData = data.randomSplit([0.7, 0.3], seed=12345) 
    trainingData = dividedData[0] #index 0 = data training
    testingData = dividedData[1] #index 1 = data testing
    maxIter = int(maxIter)
    aggregationDepth = int(aggregationDepth)
    regParam = float(regParam)
	
    assembler = VectorAssembler(inputCols = columns[:-1], outputCol="features")
    trainingDataFinal = assembler.transform(trainingData).select(col("features"), col("Cover_Type").alias("label"))
    #trainingDataFinal.show(truncate=False, n=7)
    
    testingDataFinal = assembler.transform(testingData).select(col("features"), col("Cover_Type").alias("trueLabel"))
    
    LR_model = LogisticRegression(labelCol="label",featuresCol="features",maxIter = maxIter, aggregationDepth = aggregationDepth, regParam = regParam)
    LR_model = LR_model.fit(trainingDataFinal)
    predictions = LR_model.transform(testingDataFinal)
    
    metrics = ["f1"]
    for metric in metrics:
        evaluator = MulticlassClassificationEvaluator(labelCol="trueLabel", predictionCol="prediction", metricName=metric)
        v = evaluator.evaluate(predictions)
        return v
        #print("  {}: {}".format(metric,v))
        #mlflow.log_metric(metric,v)
    
    
    # MLflow - log model
    #mlflow.spark.log_model(LR_model, "spark-logregmodel")

with mlflow.start_run() as run:
    print("MLflow:")
    print("run_id:",run.info.run_id)
    print("experiment_id:",run.info.experiment_id)
    
    
    if (args.parameter == 'maxIter'):
        aggregationDepth = 2    
        regParam = 0.0
        mlflow.log_param("aggregationDepth",aggregationDepth)
        mlflow.log_param("regParam",regParam)
    
        maxIter = np.arange(100,1100,100)
        for val in maxIter:
            f1_maxitr = train(data, val,aggregationDepth,regParam,run.info.run_id)   
            mlflow.log_metric(key="maxIter", value=f1_maxitr, step=val)
        
        
    elif (args.parameter == 'aggregationDepth'):
        maxIter = 100
        regParam = 0.0
        mlflow.log_param("maxIter",maxIter)
        mlflow.log_param("regParam",regParam)
        aggregationDepth = np.arange(2,50,4)
        for val in aggregationDepth:
            f1_aggDep = train(data, maxIter,val,regParam,run.info.run_id)   
            mlflow.log_metric(key="aggregationDepth", value=f1_aggDep, step=val)
        
        
    
    else:
        maxIter = 100                
        aggregationDepth = 2
        mlflow.log_param("maxIter",maxIter)
        mlflow.log_param("aggregationDepth",aggregationDepth)
        
        regParam = np.linspace(0,5,15)
        regParam = np.round(regParam,3)
        count = 0
        for val in regParam:
            f1_rp = train(data, maxIter,aggregationDepth,val,run.info.run_id)   
            mlflow.log_metric(key="regParam", value=f1_rp, step=count)
            count+=1
       
            
    
        
	
	
	
	
	